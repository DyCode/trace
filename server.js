'use strict';

const config = require('./config');
const express = require('express');
const app = express();
const server = require('http').createServer(app);

app.engine('html', require('./core/help/view'));
app.set('view engine', 'html');
app.set('views', __dirname + '/core/view');

app.use(express.static(__dirname + '/static'));

app.use(require('./core/route'));

server.listen(config.port, err => console.log('Listening at', config.port));
