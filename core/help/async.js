'use strict';

const Promise = require('bluebird');

module.exports = (generatorRoute) => (req, res, next) => {
  Promise.coroutines(generatorRoute)(req, res, next).catch(next);
};
