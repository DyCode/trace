'use strict';

const util = require('util');
const config = require('../../config');

if (process.env.NODE_ENV === 'production')
  module.exports = () => {};
else
  module.exports = () => {
    let loc = new Error().stack.split('\n')[2];

    // Get only path relative to project root,
    // including line cols and rows.
    loc = loc.slice(loc.indexOf(config.root) + config.root.length + 1, -1);
    console.log(loc, util.format.apply(null, arguments));
  };
