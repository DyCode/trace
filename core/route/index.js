const fs = require('fs');
const express = require('express');
const router = express.Router();
const config = require('../../config');

const land = require('../control/land');
const blag = require('../control/blag');

router.get('/', land.entry);

router.get('/blag', blag.entry);
router.get('/blag/:title', blag.single);

module.exports = router;
