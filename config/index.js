'use strict';

const path = require('path');

module.exports = require('lodash/fp/merge')({
  root: path.join(__dirname, '..'),

  port: process.env.PORT || 8000,

  secret: '',

  view: {
    cache: false,
  },

  mongodb: {
    host: 'localhost',
    port: '27017',
    database: '',
    username: '',
    password: '',
    get uri() {
      let uri = 'mongodb://';

      if (this.username && this.password)
        uri += this.username + ':' + this.password + '@';

      uri += this.host;

      if (this.port)
        uri += ':' + this.port;

      if (this.database)
        uri += '/' + this.database;

      return uri;
    },
  },

}, process.env.NODE_ENV ? require('./' + process.env.NODE_ENV) : {});
